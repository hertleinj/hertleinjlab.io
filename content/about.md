---
title: "About Me"
date: 2019-03-11T20:40:25+02:00
draft: false
tags: [cv, me]
---
 
 ♥ - Willkommen nachfolgend finden Sie einige Informationen über mich:
 <!--more-->

## Meine Kompetenzen:  
 - Java EE Stack
 - BPMN Entwurf und Realisierungen mittels der Camunda-Engine
 - Jenkins Automatisierung mit Docker
 - Angular-Frontend Entwicklung

## Eckpunkte:
 - Ausbildung & Studium an der TH-Nürnberg und der Sparda-Datenverarbeitung eG (Verbundstudium / Duales-Studium)
    - Abgeschlossene Ausbildung Fachinformatiker für Anwendungsentwicklung
    - Bachelor of Sience der Informatik mit dem Thema der Bachelorarbeit - Entwicklung einer Jenkins CI/CD Struktur
 - Software Developer - Nürnberger Versicherung AG **(aktuell)**

## Sprachen
Fließend in Schrift und Wort beherrsche ich Deutsch und Englisch. Da ich mich nie wirklich entscheiden kann, finden Sie hier auch "Posts" sowohl in Deutsch und Englisch.

## Links
 - [Xing](https://www.xing.com/profile/Jan_Hertlein2)
 - **[GitLab](https://gitlab.com/hertleinj)**
 - [GitHub](https://github.com/hertleinj)
