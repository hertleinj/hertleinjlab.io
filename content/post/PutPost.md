---
title: "REST POST vs PUT"
date: 2019-01-27T16:40:25+02:00
draft: false
tags: [coding, REST]
---
Obwohl die beiden HTTP-Methoden ähnlich klingen muss bei der Erstellung einer **Restfull** API auf die richtige Verwendung geachtet werden.

Nachfolgend kommt eine Merkliste bezüglich der beiden Methoden:

***POST:***

* Anlegen von Resourcen bei denen der Server die ID bestimmt und zurück gibt

***PUT:***

* Der Endpunkt muss resisten gegenüber einer wiederholbaren ausführung sein
* Der Identifier in der URI muss vom Server beibehalten bleiben (Client bestimmt ID)
* Ersetzten des gesamten Inhalts der bestehenden Resource

Im **Allgemeinen** sollte ***PUT*** zum ersetzten des **gesamten** Inhalts einer bestehenden Resource genutzt werdne. ***POST*** hingegen dient dem **Anlegen** von neuen Resourcen.

Noch je ein Beispiel:

```
POST   https://endpoint/api/v1/mitarbeiter
PUT    https://endpoint/api/v1/mitarbeiter/{personalnummer}
```
