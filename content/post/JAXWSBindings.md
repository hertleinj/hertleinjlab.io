---
title: "SOAP Failure WrapperStyle"
date: 2019-04-28T20:00:25+02:00
draft: false
tags: [coding, SOAP, JAX-WS, debugging, jboss, java]
---

Letzte Woche gieng nach einem eigentlich geringen Update einer bereits funktionierenden SOAP-Schnittstelle plötzlich nichts mehr. Es hieß logisch hätte sich nichts geändert nur zur Verständlichkeit wären einige Methoden und Attribute umbenannt worden. Trotz gewissenhafter Erneuerung der WSDL und XSD Inhalte und dem Ändern der Methoden zum Aufruf blieb der Aufruf des Endpunktes fehlerhaft.
<!--more-->
Als Nächstes wurde der natürlich nun erst einmal überprüft, ob die Funktionalität überhaupt bereitsteht, mittels SOAP-UI. Der Aufruf, mit dem folgenden Body (anonymisiert), war allerdings erfolgreich.
```xml
   <soapenv:Body>
      <nab:getTest>
         <ns:Key>
            <ns:Nummer>12334442</ns:Nummer>
         </ns:Key>
      </nab:getTest>
   </soapenv:Body>
```

Beim Vergleich der Fehlermeldung aus dem Server-Log mit der SOAPUI-Struktur konnte man allerdings Ähnlichkeiten erkennen:
```
javax.xml.ws.soap.SOAPFaultException: Could not parse the XML stream caused by: javax.xml.stream.XMLStreamException: cvc-elt.4.3: Type 'Key' is not validly derived from the type definition, 'getTestType', of element 'ns3:getTest'..
```

Der Fehler musste also bei meinem Service und dessen Aufbau des Body liegen. Um hier genauere Einblicke zu erhalten aktivierete ich den HTTP Dump des Servers, siehe meinen [HttLogging-Post]({{< ref "HttpLogging.md" >}}). Und siehe an Unterschiede zeigen sich:
```xml
    <soap:Body>
      <ns3:getTest xsi:type="KeyType" xmlns=...>
         <Nummer>12334442</Nummer>
      </ns3:getTest>
   </soap:Body>
```

Falls Ihr auf dasselbe Problem stoßt hier kurz und knapp zu meiner Lösung, nach einer längeren Problembeschreibung:
- Erstellen einer bindings.xjb Datei mit folgendem Inhalt:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<jaxws:bindings
        wsdlLocation="TestService-1.0.wsdl"
        xmlns="http://java.sun.com/xml/ns/jaxws"
        xmlns:jaxws="http://java.sun.com/xml/ns/jaxws">
    <enableWrapperStyle>false</enableWrapperStyle>
</jaxws:bindings>
```
- Angeben des Bindings-Files in der Plugin Konfiguration:

```xml
<bindingFiles>
    <bindingFile>bindings.xjb</bindingFile>
</bindingFiles>
```

Hoffe dies konnte Euch helfen.

