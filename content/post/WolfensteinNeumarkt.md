---
title: "Wolfstein Neumarkt"
date: 2018-05-26T11:13:36+02:00
draft: false
tags: [photografie]
---
Zum Fotolocationscouting ging es letztes Wochenende in die Opferfalz.

Die Burgruine Wolfstein liegt leicht erhöht Nordwestlich von der Stadt Neumarkt. Erreichbar ist sie entweder zu Fuß (verschiedene Streckenlängen möglich) oder direkt mit dem Auto.

Viel Spaß beim Ansehen der dabei entstandenen Bilder.
<!--more-->
{{<olg src="/img/DSF2777.jpg" width="80%">}}
