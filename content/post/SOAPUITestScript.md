---
title: "SOAP-UI Testergebnisse in Textfile ablegen"
date: 2019-04-30T11:03:32+02:00
draft: false
tags: [coding, SOAP, JAX-WS, java]
---

Funktionstests müssen vor allem in großen Projekten auf allen Infratruktur-Ebenen wiederholt werden. Die Ergebnisse dieser sollten auch nachvollziehbar und einheitlich dokumentiert werden. <!--more--> Wenn man für die Test der Service-Schnittestellen [SOAP-UI](https://www.soapui.org/) verwendet kann man das nachfolgende Skript verwenden um die Ergebnisse der TestCases / Teststeps in Textfiles zu hinterlegen. 

Hierfür müsst Ihr einfach nachfolgendes im Teardown-Script Feld eintragen:
```groovy
def testSteps = context.testCase.getTestStepList() 
testSteps.each{
    def request = context.testCase.getTestStepByName(it.name).getPropertyValue('request')
    def endpoint = context.testCase.getTestStepByName(it.name).getPropertyValue('endpoint')
    def result = context.testCase.getTestStepByName(it.name).getPropertyValue('result')
    def response = context.testCase.getTestStepByName(it.name).getPropertyValue('response')
    def tclog = new File ("C:\\Users\\B070546\\temp\\"+it.name+".txt")
    
    tclog << endpoint 
    tclog << "\r\n Request \r\n"
    tclog << request
    tclog << "\r\n Response \r\n"
    tclog << response
    tclog << "\r\n"
    tclog << "\r\n"
    tclog << new Date().format("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
}
```

