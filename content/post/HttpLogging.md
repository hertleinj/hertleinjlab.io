---
title: "Java-Applicationserver HTTP Logging"
date: 2019-03-11T20:40:25+02:00
draft: false
tags: [coding, HTTP, debugging, jboss, java]
---
Beim Auftreten von "Fehlern" bei der synchronen Kommuniktion zwischern Servicen ist oft die erste Frage schon entscheident über die Dauer wie lange man für die Feherlanalyse. Entscheident kann hier ein Einblick in die unverpackte HTTP-Kommunikation bringen.
<!--more-->
Hier eine beispielhafte Konfigruation eines JBoss Applicationservers unter Windows. Hier in die standalone.conf.bat folgende Zeile einfügen:

```bash
...
rem # Log Http
set "JAVA_OPTS=%JAVA_OPTS%  -Dcom.sun.xml.ws.transport.http.client.HttpTransportPipe.dump=true"
...
```

Beim Start des Servers sollte unter JAVA_OPTS dies dann mit angegeben sein:

```bash
===============================================================================
  JBoss Bootstrap Environment
  JBOSS_HOME: "D:\Werkzeuge\XXXX\jboss-eap-7.0.9\jboss-eap-7.0.9"
  JAVA: "C:\Program Files\Java\jdk1.8.0_161\bin\java"
  JAVA_OPTS: "...  -Dcom.sun.xml.ws.transport.http.client.HttpTransportPipe.dump=true ..."
===============================================================================
```

Das eigentliche Logging von HTTP-Requests besteht immer aus 2 Teilen dem Request und der Response, dies kann nun getestet werden und sollte wie folgt aussehen:

```xml-dtd
19:02:49,706 INFO  [org.apache.cxf.services.XXXPortType] (default task-13) Outbound Message
---------------------------
ID: 2
Address: http://webservices/services/XXXService
Encoding: UTF-8
Http-Method: POST
Content-Type: text/xml
Headers: {Accept=[*/*], SOAPAction=["urn:SearchXXX"]}
Payload: <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header>...</soap:Header><soap:Body>...Request...</soap:Body></soap:Envelope>
--------------------------------------
19:02:50,847 INFO  [org.apache.cxf.services.XXXPortType] (default task-13) Inbound Message
----------------------------
ID: 2
Response-Code: 200
Encoding: UTF-8
Content-Type: text/xml;charset=UTF-8
Headers: {connection=[Keep-Alive], Content-Length=[755], content-type=[text/xml;charset=UTF-8], Date=[Tue, 12 Mar 2019 19:02:49 GMT], Keep-Alive=[timeout=5, max=50], Server=[XXX-Server]}
Payload: <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"/><soap:Body>...Response...</soap:Body></soap:Envelope>
--------------------------------------
```



