---
title: "One Line Gallery"
date: 2018-10-27T16:40:25+02:00
draft: false
tags: [coding, hugo, go]
---

Für meine Fotoblog Einträge wollte ich eine einfache und simple Galerie um auch mehrere Bilder nebeneinander anzeigen zu können. Da ich für meinen Blog auf [Hugo](https://gohugo.io/) setzte. Habe ich meinen eigenen kleinen Shortcode geschrieben.
<!--more-->
Beispielhafte Anwendungen:

```
{{ <olg src="/img/olg/16-9.png,/img/olg/1-1.jpg,/img/olg/16-9.png" width="80%">}}
```

{{<olg src="/img/olg/16-9.png,/img/olg/1-1.jpg,/img/olg/16-9.png" width="80%">}}


```
{{ <olg src="/img/olg/16-9.png,/img/olg/16-9.png,/img/olg/1-1.jpg" width="80%" elwidth="48">}}
```

{{<olg src="/img/olg/16-9.png,/img/olg/16-9.png,/img/olg/1-1.jpg" width="80%" elwidth="48">}}

```
{{ <olg src="/img/olg/9-16.png,/img/olg/16-9.png" width="80%">}}
```
{{<olg src="/img/olg/9-16.png,/img/olg/16-9.png" width="80%">}}

Hier denke ich noch an einer verbesserten Konfigurierbarkeit arbeiten mit der jedem Element eine Breite zugewiesen werden kann. Für Verbesserungen / Verbesserungsvorschläge findet ihr den Code unter dem [Gitlab-Repo](https://gitlab.com/hertleinj/one-line-gallery).