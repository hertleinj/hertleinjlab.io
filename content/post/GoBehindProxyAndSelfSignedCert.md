---
title: "Go Self Signed Cert"
date: 2019-05-28T16:14:43+02:00
draft: false
tags: [coding, Golang]
---

In großen Unternehmen steht zwischen dem Entwickler und dem "freien" Internet oft mehrere "Hürden". Hier sind Artifactory, Proxy und Zertifikate dem Entwickler oft ein Dorn im Auge. Vor allem bei Tooling zu relativ "neuen" Sprachen benötigt das Setup dadurch etwas mehr Zeit. Im Nachfolgenden gehe ich kurz durch wie Ihr für Golang Zertifikat einrichtet. 
<!--more-->

Um ```go get``` ausführen zu können muss man nicht direkt die Golang Config bearbeiten. Die Einstellungen der Git Installation dort müssen wir folgende Einstellungen global setzten:

- http.sslCAInfo 
- http.sslCAPath

Wobei Info auf das Base64 Zertifikat direkt zeigen muss und Path auf das Verzeichnis.