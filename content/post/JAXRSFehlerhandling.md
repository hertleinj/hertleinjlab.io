---
title: "JAX-RS Client Gewohnheitsfalle HTTP-Fehler"
date: 2019-07-11T23:40:25+02:00
draft: false
tags: [coding, JAX-RS, REST, java]
---

Bei der Entwicklung eines Produktes mit Java bedient man sich zum Aufruf von REST-Schnittstellen der wirklich angenehmen und einfach verständlichen JAX-RS Syntax. Hierbei kann sich aber im Punkt Fehlerbehandlung ein Gewohnheitsfehler leicht einschleichen.
<!--more-->
Da man bei der Entwicklung zu meist sich zuerst einmal viele Informationen von anderen Schnittstellen einholt, verwendet man vorerst folgenden Syntax:
```java
try {
    Customer customer = client.target("http://myshop.de/product/421")
                              .accept("application/json")
                              .get(Product.class);
} catch (NotFoundException notFound) {
  ...
}
```
Diese ist auch voll funktionstüchtig und man erhält bei einem HTTP Fehler Code [die entsprechende Exception](https://dennis-xlc.gitbooks.io/restful-java-with-jax-rs-2-0-en/cn/part1/chapter7/exception_handling.html). 

Dies funktioniert bei allen Schnittstellen-Methoden wunderbar welche auch ein **Objekt im Body** der Response zurückgeben und umgewandelt wird. Bei Methoden **ohne Response Objekt** lässt einen die implizite Fehlerbehandlung im Stich, er werden **keine** Exceptions geworfen. 

Deshalb muss man wie folgt den Return-Code selbst prüfen:
```java
Response response = client.target("http://myshop.de/product/421/delete")
                          .accept("application/json")
                          .post();
try {
   if (response.getStatus() != 200) {
      throw new WebApplicationException("uuid-1", response.getStatus());
   }
} finally {
  response.close();
}
```